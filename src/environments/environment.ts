// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBXWobLUM_B0gt2Z4xDgaRzfLDaVNmoCZE',
    authDomain: 'mini-twitter-dc24c.firebaseapp.com',
    databaseURL: 'https://mini-twitter-dc24c.firebaseio.com',
    projectId: 'mini-twitter-dc24c',
    storageBucket: 'mini-twitter-dc24c.appspot.com',
    messagingSenderId: '616911596889',
    appId: '1:616911596889:web:e144ff0449e89be9767048'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
