import { Injectable } from '@angular/core';
import {AngularFirestore, DocumentReference} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import * as moment from 'moment';
import 'moment/locale/es';


@Injectable({
  providedIn: 'root'
})
export class FirestoreService {
  posts: Array<object>;
  docRef: any;
  constructor(private firestore: AngularFirestore, private angularFireAuth: AngularFireAuth) { }

  public async getUser(): Promise<object> {
    // @ts-ignore
    const authUser = await this.angularFireAuth.auth.currentUser;
    let user: object = {};
    await this.firestore.collection('users').doc(authUser.uid).ref.get().then(doc => {
      user = Object.assign({id: doc.id}, doc.data());
    }).catch(error => {
      console.log(error);
    });
    return user;
  }
  public async verifyUserExist(email: string) {
    const querySnapshot = await this.firestore.collection('users').ref.where('email', '==', email).get();
    return  !querySnapshot.empty;
  }
  // Crea un nuevo usuario
  public createUser(id: string, data: object) {
    return this.firestore.collection('users').doc(id).set(data);
  }
  // Actualiza un usuario
  public updateUser(id: string, data: any) {
    return this.firestore.collection('users').doc(id).update(data);
  }
  // Elimina un usuario
  /*public deleteUser(id: string) {
    return this.firestore.collection('users').doc(id).delete();
  }*/
  public formatCorrectDateForAllBrowsers(date): string {
    date = date.replace(/-/g, '/');  // replaces all occurances of "-" with "/"
    const dateObject = new Date(date);
    return dateObject.toDateString();
  }
  // Obtiene todos los posts
  public async getPosts(): Promise<Array<object>> {
    const posts: Array<object> = [];
    let comments: Array<object> = [];
    this.docRef = this.firestore.collection('posts');
    await this.docRef.ref.orderBy('created_at', 'desc').get().then(async doc => {
      for  (const snapshot of doc.docs) {
        const commentsRef = this.firestore.collection('posts').doc(snapshot.id).collection('comments');
        await commentsRef.ref.orderBy('created_at', 'asc').get().then(commentSnapshot => {
          for (const comment of commentSnapshot.docs) {
            const formattedComment = comment.data();
            formattedComment.created_at = this.formatCorrectDateForAllBrowsers(formattedComment.created_at);
            formattedComment.created_at = moment(new Date(formattedComment.created_at)).fromNow();
            comments.push(Object.assign({id: comment.id}, formattedComment));
          }
        });
        const formattedPost = snapshot.data();
        formattedPost.created_at = this.formatCorrectDateForAllBrowsers(formattedPost.created_at);
        formattedPost.created_at = moment(new Date(formattedPost.created_at)).fromNow();
        posts.push(Object.assign({id: snapshot.id, comments}, formattedPost));
        comments = [];
      }
      this.posts = posts;
    });
    return this.posts;
  }

  // Crea un nuevo post
  public createPost(data: object): Promise<DocumentReference> {
    return this.firestore.collection('posts').add(data);
  }
  public updatePost(id: string, data: any) {
    return this.firestore.collection('posts').doc(id).update(data);
  }
  public addComment(id: string, data: object): Promise<DocumentReference> {
    return this.firestore.collection('posts').doc(id).collection('comments').add(data);
  }
  public deletePost(id: string) {
    return this.firestore.collection('posts').doc(id).delete();
  }
}
