import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';


@Injectable({
  providedIn: 'root'
})
export class FirebaseStorageService {

  constructor(
    private storage: AngularFireStorage,
  ) { }

  // Tarea para subir archivo
  public taskCloudStorage(path: string, nameFile: string, data: any) {
    const filePath = `${path}/${nameFile}`;
    const task = this.storage.upload(filePath, data);
    return task;
  }

  // Referencia del archivo
  public refCloudStorage(path: string, nameFile: string) {
    return this.storage.ref(`${path}/${nameFile}`);
  }
  public deleteFile(url) {
    return this.storage.storage.refFromURL(url).delete();
  }
}
