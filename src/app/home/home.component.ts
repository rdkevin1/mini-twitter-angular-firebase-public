import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AddPostModalComponent} from '../add-post-modal/add-post-modal.component';
import {ToastrService} from 'ngx-toastr';
import {FirestoreService} from '../services/firestore/firestore.service';
import { faEdit, faTrash, faHeart} from '@fortawesome/free-solid-svg-icons';
import { faHeart as faHeartEmpty } from '@fortawesome/free-regular-svg-icons';
import * as moment from 'moment';
import 'moment/locale/es';
import Swal from 'sweetalert2';
import {UpdatePostModalComponent} from '../update-post-modal/update-post-modal.component';
import {FirebaseStorageService} from '../services/firestorage/firebase-storage.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private firestore: FirestoreService,
    private firebaseStorage: FirebaseStorageService
  ) { }
  userInfo: any = null;
  postModalTemplate: TemplateRef<any>;
  @ViewChild(AddPostModalComponent, { static: true }) PostModal: AddPostModalComponent;
  updatePostModalTemplate: TemplateRef<any>;
  @ViewChild(UpdatePostModalComponent, { static: true }) UpdatePostModal: UpdatePostModalComponent;
  posts: Array<any> = [];
  commentPost: string = '';
  faEdit: any = faEdit;
  faTrash: any = faTrash;
  faHearthEmpty: any = faHeartEmpty;
  faHearth: any = faHeart;
  isLoadingComment: boolean = false;
  isLoadingLike: boolean = false;
  ngOnInit(): void {
    this.getUser();
    this.getPosts();
  }
  async getUser() {
    this.userInfo = await this.firestore.getUser();
  }
  async getPosts() {
   this.posts = await this.firestore.getPosts();
  }
  showPostModal() {
    this.postModalTemplate = this.PostModal.postModalTemplate;
    this.PostModal.userInfo = this.userInfo;
    this.PostModal.showModal(this.postModalTemplate);
  }
  showUpdateModal(post) {
    this.updatePostModalTemplate = this.UpdatePostModal.updatePostModalTemplate;
    this.UpdatePostModal.showUpdateModal(this.updatePostModalTemplate, post);
  }
   addPostInRenderPost(event): void {
    event.created_at = this.firestore.formatCorrectDateForAllBrowsers(event.created_at);
    event.created_at = moment(event.created_at).fromNow();
    this.posts.unshift(event);
   }
   updateRenderPost(event): void {
    const index = this.posts.findIndex(post => {
      return post.id === event.id;
    });
    this.posts[index].extract = event.params.extract;
    this.posts[index].updated_at = event.params.updated_at;
  }
  async addCommentToRenderPost(postId, params) {
    params.created_at = this.firestore.formatCorrectDateForAllBrowsers(params.created_at);
    params.created_at =  moment(params.created_at).fromNow();
    const index = this.posts.findIndex(post => {
      return post.id === postId;
    });
    this.posts[index].comments.push(params);
  }
  async addComment(post: any) {
    this.isLoadingComment = true;
    const params: object = {
      comment: this.commentPost,
      created_at: moment().format('YYYY-MM-DD hh:mm a'),
      updated_at: moment().format('YYYY-MM-DD hh:mm a'),
      pictureUrl: this.userInfo.pictureUrl,
      user_id: this.userInfo.id,
      username: this.userInfo.username
    };
    await this.firestore.addComment(post.id, params).then((doc) => {
      this.addCommentToRenderPost(post.id, params);
      this.commentPost = '';
      this.isLoadingComment = false;
    }).catch((error) => {
      console.log(error);
      Swal.fire({
        title: 'Ocurrio un error al comentar',
        text: 'Intentelo nuevamente',
        icon: 'error',
        confirmButtonText: 'Aceptar'
      });
    });
  }
  deletePostInRenderPosts(postId): void {
    const posts = this.posts.filter(post => {
      if (post.id === postId) {
        return false;
      } else {
        return true;
      }
    });
    this.posts = posts;
  }
  async deletePost(post: any): Promise<void> {
    Swal.fire({
      title: 'Se eliminara el post',
      text: '¿Estas seguro? esta accion no puede deshacerse',
      icon: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Confirmar'
    }).then(async (isConfirm) => {
      if (isConfirm.value) {
        await this.firestore.deletePost(post.id).then((doc) => {
          this.deletePostInRenderPosts(post.id);
          this.toastr.info('Tu post ha sido eliminado');
        }).catch((error) => {
          console.log(error);
        });
        if (post.multimediaUrl !== null) {
          await this.firebaseStorage.deleteFile(post.multimediaUrl);
        }
      } else {
        this.toastr.info('Cancelado...');
      }
    });
  }
  verifyIfLikeExist(post) {
    const likes = post.likes;
    if (likes.length > 0) {
      const findLike = likes.find(like => {
        return like.user_id === this.userInfo.id;
      });
      if (findLike !== undefined) {
        return true;
      }
    }
    return false;
  }
  async unLike(post: any) {
    this.isLoadingLike = true;
    const likes = post.likes.filter(like => {
      if (like.user_id === this.userInfo.id) {
        return false;
      } else {
        return true;
      }
    });
    post.likes = likes;
    await this.firestore.updatePost(post.id, {likes}).then((doc) => {
      this.isLoadingLike = false;
    }).catch((error) => {
      console.log(error);
    });
  }
  async addLike(post: any) {
    const likeExist = this.verifyIfLikeExist(post);
    if (likeExist) {
      await this.unLike(post);
    } else {
      this.isLoadingLike = true;
      const likes = post.likes;
      likes.push({user_id: this.userInfo.id, username: this.userInfo.username, pictureUrl: this.userInfo.pictureUrl});
      await this.firestore.updatePost(post.id, {likes}).then((doc) => {
        this.isLoadingLike = false;
      }).catch((error) => {
        console.log(error);
      });
    }
  }
}
