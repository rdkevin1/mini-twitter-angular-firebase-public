import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {FirestoreService} from '../services/firestore/firestore.service';
import Swal from 'sweetalert2';
import {finalize} from 'rxjs/operators';
import {FirebaseStorageService} from '../services/firestorage/firebase-storage.service';
import * as moment from 'moment';

@Component({
  selector: 'app-add-post-modal',
  templateUrl: './add-post-modal.component.html',
  styleUrls: ['./add-post-modal.component.scss']
})
export class AddPostModalComponent implements OnInit {

  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private firestore: FirestoreService,
    private firebaseStorage: FirebaseStorageService
  ) { }
  modalRef: BsModalRef;
  postForm: FormGroup;
  multimedia: any = null;
  userInfo: any;
  percentage: number = 0;
  isLoading: boolean = false;
  correctFile: boolean = true;
  @Output() addPostEvent = new EventEmitter();
  @ViewChild('postModalTemplate') postModalTemplate: TemplateRef<any>;
  ngOnInit(): void {
    this.createPostForm();
  }
  createPostForm(): void {
    this.postForm = this.formBuilder.group({
      extract:        ['', Validators.required],
      multimediaType: [null],
      multimediaUrl:  [null],
      created_at:     [moment().format('YYYY-MM-DD hh:mm a')],
      updated_at:     [moment().format('YYYY-MM-DD hh:mm a')],
      transmitter:    [null],
      likes:          [[]]
    });
  }
  showModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  clearModal() {
    this.postForm.reset();
    this.multimedia = null;
  }
  closeModal() {
    this.modalRef.hide();
  }
  onFileChange(event): void {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      if (file.type === 'image/jpeg' || file.type === 'image/png' || file.type === 'video/mp4') {
        this.correctFile = true;
        this.multimedia = file;
        this.postForm.controls.multimediaType.setValue(file.type);
      } else {
        this.correctFile = false;
        Swal.fire({
          title: 'Error!',
          text: 'El archivo debe ser formato jpeg o png',
          icon: 'error',
          confirmButtonText: 'Aceptar'
        });
      }
    }
  }
  validateIfMultimediaCharge() {
    if (this.multimedia !== null) {
      this.uploadMultimediaPostInFirebaseStorage();
    } else {
      this.addPost();
    }
  }
  async uploadMultimediaPostInFirebaseStorage() {
    this.isLoading = true;
    const postMultimediaRef = this.firebaseStorage.refCloudStorage('posts', this.multimedia.name);
    const taskUpload =  this.firebaseStorage.taskCloudStorage('posts', this.multimedia.name, this.multimedia);
    await taskUpload.percentageChanges().subscribe((percentage) => {
      this.percentage = Math.round(percentage);
    });
    taskUpload.snapshotChanges().pipe(
      finalize(async () => {
        await postMultimediaRef.getDownloadURL().subscribe(async (url) => {
          this.postForm.controls.multimediaUrl.setValue(url ? url : '');
          this.addPost();
        });
      })
    ).subscribe();
  }
  async addPost(): Promise<void> {
    this.isLoading = true;
    const transmitter: object = {
      id: this.userInfo.id,
      username: this.userInfo.username,
      pictureUrl: this.userInfo.pictureUrl
    };
    this.postForm.controls.transmitter.setValue(transmitter);
    this.postForm.controls.created_at.setValue(moment().format('YYYY-MM-DD hh:mm a'));
    this.postForm.controls.updated_at.setValue(moment().format('YYYY-MM-DD hh:mm a'));
    this.postForm.controls.likes.setValue([]);
    await this.firestore.createPost(this.postForm.value).then((doc) => {
      this.toastr.success('Tu post fue publicado con exito');
      this.addPostEvent.emit(Object.assign({id: doc.id, comments: []}, this.postForm.value));
    }).catch((error) => {
      console.log(error);
    }).finally(() => {
      this.closeModal();
      this.clearModal();
      this.percentage = 0;
      this.isLoading = false;
    });
  }
}
