import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {FirestoreService} from '../services/firestore/firestore.service';
import * as moment from 'moment';

@Component({
  selector: 'app-update-post-modal',
  templateUrl: './update-post-modal.component.html',
  styleUrls: ['./update-post-modal.component.scss']
})
export class UpdatePostModalComponent implements OnInit {

  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private firestore: FirestoreService
  ) { }
  id: string;
  modalRef: BsModalRef;
  postUpdateForm: FormGroup;
  isLoading: boolean = false;
  @Output() postUpdateEvent = new EventEmitter();
  @ViewChild('updatePostModalTemplate') updatePostModalTemplate: TemplateRef<any>;

  ngOnInit(): void {
    this.createUpdatePostForm();
  }
  createUpdatePostForm(): void {
    this.postUpdateForm = this.formBuilder.group({
      extract:    ['', Validators.required],
      updated_at:  [moment().format('YYYY-MM-DD hh:mm a')]
    });
  }
  showUpdateModal(template: TemplateRef<any>, params: object) {
    this.chargeModalWithParams(params);
    this.modalRef = this.modalService.show(template);
  }
  chargeModalWithParams(params) {
    this.id = params.id;
    this.postUpdateForm.controls.extract.setValue(params.extract);
  }
  clearModal() {
    this.postUpdateForm.reset();
  }
  closeModal() {
    this.modalRef.hide();
  }
  async updatePost() {
    this.isLoading = true;
    await this.firestore.updatePost(this.id, this.postUpdateForm.value).then((doc) => {
      this.toastr.success('Post actualizado');
      this.postUpdateEvent.emit({id: this.id, params: this.postUpdateForm.value});
    }).catch((error) => {
      console.log(error);
      this.toastr.error('Hubo un error al actualizar el post, intentelo de nuevo');
    }).finally(() => {
      this.closeModal();
      this.isLoading = false;
      this.clearModal();
    });
  }

}
