import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private formBuilder: FormBuilder, private angularFireAuth: AngularFireAuth) {}
  loginForm: FormGroup;
  isError: boolean = false;
  errorMsg: string = '';
  ngOnInit(): void {
    this.createLoginForm();
  }
  createLoginForm(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }
  async login(): Promise<void> {
    await this.angularFireAuth.auth.signInWithEmailAndPassword(this.loginForm.value.email, this.loginForm.value.password).then(response => {
      this.router.navigateByUrl('/home');
    }).catch(error => {
      console.log(error);
      this.isError = true;
      this.errorMsg = error.code;
      setTimeout(() => {
        this.isError = false;
      }, 5000);
    });
  }

}
