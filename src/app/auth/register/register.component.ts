import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import {AngularFireAuth} from '@angular/fire/auth';
import {FirebaseStorageService} from '../../services/firestorage/firebase-storage.service';
import {finalize} from 'rxjs/operators';
import {FirestoreService} from '../../services/firestore/firestore.service';
import * as moment from 'moment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private angularFireAuth: AngularFireAuth,
    private toastr: ToastrService,
    private firestore: FirestoreService,
    private firebaseStorage: FirebaseStorageService
  ) { }
  registerForm: FormGroup;
  picture: any = null;
  percentage: number = 0;
  isLoading: boolean = false;
  ngOnInit(): void {
    this.createRegisterForm();
  }
  createRegisterForm() {
    this.registerForm = this.formBuilder.group({
      username:     ['', Validators.required],
      birthdate:    ['', Validators.required],
      city:         ['', Validators.required],
      description:  ['', Validators.required],
      email:        ['', Validators.compose([Validators.email, Validators.required])],
      password:     ['', Validators.compose([Validators.minLength(6), Validators.required])],
      pictureUrl:   ['', Validators.required],
      created_at:   [moment().format('YYYY-MM-DD')],
      updated_at:   [moment().format('YYYY-MM-DD')]
    });
  }
  onFileChange(event): void {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        this.picture = file;
        this.registerForm.controls.pictureUrl.setValue(file ? file.name : '');
      } else {
        Swal.fire({
          title: 'Error!',
          text: 'El archivo debe ser formato jpeg o png',
          icon: 'error',
          confirmButtonText: 'Aceptar'
        });
      }
    }
  }
  async validateIfEmailAlreadyTaken() {
    const emailAlreadyTaken = await this.firestore.verifyUserExist(this.registerForm.value.email);
    if (emailAlreadyTaken) {
      Swal.fire({
        title: 'Este correo ya esta en uso, utiliza otro.',
        icon: 'error',
        confirmButtonText: 'Aceptar'
      });
    } else {
      this.uploadProfilePictureInStorage();
    }
  }
  async uploadProfilePictureInStorage(): Promise<void> {
    this.isLoading = true;
    const pictureImageRef = this.firebaseStorage.refCloudStorage('users', this.picture.name);
    const taskUpload =  this.firebaseStorage.taskCloudStorage('users', this.picture.name, this.picture);
    await taskUpload.percentageChanges().subscribe((percentage) => {
      this.percentage = Math.round(percentage);
    });
    taskUpload.snapshotChanges().pipe(
      finalize(async () => {
        await pictureImageRef.getDownloadURL().subscribe((url) => {
          this.registerForm.controls.pictureUrl.setValue(url ? url : '');
          this.register();
        });
      })
    ).subscribe();
  }
  async register(): Promise<void> {
    const email: string = this.registerForm.value.email;
    const password: string = this.registerForm.value.password;
    await this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password).then((response) => {
      this.firestore.createUser(response.user.uid, this.registerForm.value);
      this.toastr.success('Sesion iniciada', 'Registro exitoso');
      this.router.navigateByUrl('/login');
    }).catch((error) => {
      console.log(error);
      Swal.fire({
        title: 'Ocurrio un error al registrar, intentelo de nuevo',
        icon: 'error',
        confirmButtonText: 'Aceptar'
      });
    }).then(() => {
      this.isLoading = false;
    });
  }

}
