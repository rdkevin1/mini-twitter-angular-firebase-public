import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {Router} from '@angular/router';
import {User} from 'firebase';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  loadingState: boolean = true;
  loggedIn: boolean = false;
  user: User;
  constructor(private router: Router, private angularFireAuth: AngularFireAuth) {
    this.angularFireAuth.user.subscribe((user) => {
      this.user = user;
      if (this.user !== null) {
        this.loggedIn = true;
      } else {
        this.loggedIn = false;
      }
    });
    setTimeout(() => {
      this.loadingState = false;
    }, 1000);
  }

  ngOnInit(): void {}
  async logout(): Promise<void> {
    await this.angularFireAuth.auth.signOut();
    await this.router.navigateByUrl('/');
  }

}
