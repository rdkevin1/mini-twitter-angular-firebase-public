import {Component, EventEmitter, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';
import {FirestoreService} from '../services/firestore/firestore.service';
import * as moment from 'moment';

@Component({
  selector: 'app-update-profile-modal',
  templateUrl: './update-profile-modal.component.html',
  styleUrls: ['./update-profile-modal.component.scss']
})
export class UpdateProfileModalComponent implements OnInit {

  constructor(
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private firestore: FirestoreService
  ) { }
  id: string;
  modalRef: BsModalRef;
  userUpdateForm: FormGroup;
  isLoading: boolean = false;
  @Output() userUpdateEvent = new EventEmitter();
  @ViewChild('updateModalTemplate') updateUserModalTemplate: TemplateRef<any>;

  ngOnInit(): void {
    this.createUpdateUserForm();
  }
  createUpdateUserForm(): void {
    this.userUpdateForm = this.formBuilder.group({
      username:    ['', Validators.required],
      description: ['', Validators.required],
      birthdate:   ['', Validators.required],
      city:        ['', Validators.required],
      updated_at:  [moment().format('YYYY-MM-DD hh:mm a')]
    });
  }
  showUpdateModal(template: TemplateRef<any>, params: object) {
    this.chargeModalWithParams(params);
    this.modalRef = this.modalService.show(template);
  }
  chargeModalWithParams(params) {
    this.id = params.id;
    this.userUpdateForm.controls.username.setValue(params.username);
    this.userUpdateForm.controls.description.setValue(params.description);
    this.userUpdateForm.controls.birthdate.setValue(params.birthdate);
    this.userUpdateForm.controls.city.setValue(params.city);
  }
  clearModal() {
    this.userUpdateForm.reset();
  }
  closeModal() {
    this.modalRef.hide();
  }
  async updateUser() {
    this.isLoading = true;
    await this.firestore.updateUser(this.id, this.userUpdateForm.value).then((doc) => {
      this.toastr.success('Informacion actualizada');
      this.userUpdateEvent.emit();
    }).catch((error) => {
      console.log(error);
      this.toastr.error('Hubo un error al actualizar, intentelo de nuevo');
    }).finally(() => {
      this.closeModal();
      this.isLoading = false;
      this.clearModal();
    });
  }

}
