import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { faEdit, faCamera } from '@fortawesome/free-solid-svg-icons';
import {FirestoreService} from '../services/firestore/firestore.service';
import Swal from 'sweetalert2';
import {finalize} from 'rxjs/operators';
import {FirebaseStorageService} from '../services/firestorage/firebase-storage.service';
import {UpdateProfileModalComponent} from '../update-profile-modal/update-profile-modal.component';


// @ts-ignore
@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  userInfo: any = null;
  faEdit: any = faEdit;
  faCamera: any = faCamera;
  picture: any = null;
  percentage: number = 0;
  updateLoading: boolean = false;
  updateUserModalTemplate: TemplateRef<any>;
  @ViewChild(UpdateProfileModalComponent, { static: true }) UpdateProfileModal: UpdateProfileModalComponent;
  constructor(
    private firestore: FirestoreService,
    private firebaseStorage: FirebaseStorageService
  ) { }

  ngOnInit(): void {
    this.getUserInfo();
  }
  async getUserInfo() {
    this.userInfo = await this.firestore.getUser();
  }
  showUpdateModal() {
    this.updateUserModalTemplate = this.UpdateProfileModal.updateUserModalTemplate;
    this.UpdateProfileModal.showUpdateModal(this.updateUserModalTemplate, this.userInfo);
  }
  onFileChange(event): void {
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      if (file.type === 'image/jpeg' || file.type === 'image/png') {
        this.picture = file;
        this.uploadUpdateProfilePictureInStorage();
      } else {
        Swal.fire({
          title: 'Error!',
          text: 'El archivo debe ser formato jpeg o png',
          icon: 'error',
          confirmButtonText: 'Aceptar'
        });
      }
    }
  }
  async uploadUpdateProfilePictureInStorage(): Promise<void> {
    this.updateLoading = true;
    const pictureImageRef = this.firebaseStorage.refCloudStorage('users', this.picture.name);
    const taskUpload =  this.firebaseStorage.taskCloudStorage('users', this.picture.name, this.picture);
    await taskUpload.percentageChanges().subscribe((percentage) => {
      this.percentage = Math.round(percentage);
    });
    taskUpload.snapshotChanges().pipe(
      finalize(async () => {
        await pictureImageRef.getDownloadURL().subscribe(async (url) => {
          await this.firestore.updateUser(this.userInfo.id, {pictureUrl: url}).then((doc) => {
            this.userInfo.pictureUrl = url;
            this.updateLoading = false;
            this.percentage = 0;
          }).catch((error) => {
            Swal.fire({
              title: 'Ocurrio un error actualizando la imagen',
              text: 'Intentelo de nuevo',
              icon: error,
              confirmButtonText: 'Aceptar'
            });
          });
        });
      })
    ).subscribe();
  }
}
