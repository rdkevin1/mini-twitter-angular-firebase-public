import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthGuard, AngularFireAuthGuardModule} from '@angular/fire/auth-guard';
import {AngularFirestore, AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuth, AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireStorage, AngularFireStorageModule} from '@angular/fire/storage';
import {BsModalRef, ModalModule} from 'ngx-bootstrap/modal';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { Page404Component } from './page404/page404.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IndexComponent } from './index/index.component';
import { LoginComponent } from './auth/login/login.component';
import { HomeComponent } from './home/home.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { RegisterComponent } from './auth/register/register.component';
import { ProfileComponent } from './profile/profile.component';
import {NgCircleProgressModule} from 'ng-circle-progress';
import { UpdateProfileModalComponent } from './update-profile-modal/update-profile-modal.component';
import { AddPostModalComponent } from './add-post-modal/add-post-modal.component';
import { UpdatePostModalComponent } from './update-post-modal/update-post-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    NavbarComponent,
    IndexComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    ProfileComponent,
    UpdateProfileModalComponent,
    AddPostModalComponent,
    UpdatePostModalComponent,
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgCircleProgressModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFireAuthGuardModule
  ],
  providers: [
    AngularFirestore,
    AngularFireAuth,
    AngularFireAuthGuard,
    AngularFireStorage,
    BsModalRef
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
